js.bootstrap_colorpicker
========================

.. contents::

Introduction
------------

This library packages `Colorpicker for Bootstrap`_ for `fanstatic`_.

.. _`fanstatic`: http://fanstatic.org
.. _`Colorpicker for Bootstrap`: http://www.eyecon.ro/bootstrap-colorpicker/

This requires integration between your web framework and ``fanstatic``,
and making sure that the original resources (shipped in the ``resources``
directory in ``js.bootstrap_colorpicker``) are published to some URL.
